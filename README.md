# Social Microbes

## Description
Public engagement project with Dr. Elze Hesse, including a residency series (2023) and an AccessLab workshop (2024). This repository is just for project management, not software.
